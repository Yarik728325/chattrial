import React from 'react';
import './style.scss';
import Botton from '../Botton';
import PeopleList from '../PeopleList';

const LeftBar = ({ name }) => {
  return (
    <div className="LeftBar">
      <div className="headerLeftBar">
        <div className="wrapperLogIn">
          <img src="https://sample.quickblox.com/chat/img/qb-logo.svg" alt="" />
          <b>Log in as {name}</b>
        </div>
        <Botton text="Show All Users" />
      </div>
      <PeopleList />
    </div>
  );
};
export default LeftBar;
