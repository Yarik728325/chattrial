import React from 'react';
import './style.scss';
import { useDispatch, useSelector } from 'react-redux';
import { createChat } from '@/redux/actions/deteils';

const PeopleListDeteil = ({ data, index }) => {
  const { indexUser } = useSelector((state) => state.othercontacts);
  let ownClass = '';
  if (indexUser === index) {
    ownClass = 'others';
  } else {
    ownClass = 'name';
  }
  const dispatch = useDispatch();
  const myCheck = (id) => {
    dispatch(createChat({ id, index }));
  };
  return (
    <div className="PeopleListDeteil_wrapper">
      <button
        className={ownClass}
        onClick={() => myCheck(data.id)}
        type="button"
      >
        {data.full_name}:({data.login})
      </button>
    </div>
  );
};
export default PeopleListDeteil;
