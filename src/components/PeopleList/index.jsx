import React from 'react';
import { useSelector } from 'react-redux';
import PeopleListDeteil from '../PeopleListDeteil';
import './style.scss';

const PeopleList = () => {
  const data = useSelector((state) => state.othercontacts);
  if (data.loading) {
    return <h1>Loading ...</h1>;
  }
  let { users } = data;
  users = users.map((e, index) => {
    return (
      <PeopleListDeteil
        key={Math.random().toString(36).substring(7)}
        data={e.user}
        index={index}
      />
    );
  });
  return <div className="checker">{users}</div>;
};

export default PeopleList;
