import React, { useRef, useEffect } from 'react';
import './style.scss';
import { useSelector, useDispatch } from 'react-redux';
import { SendMessage, getMessage } from '@/redux/actions/deteils';
import QuickBlox from 'quickblox/quickblox.min';
import MessageItem from '../MessageItem';

const RightBar = () => {
  const dispatch = useDispatch();
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView(false);
  };
  useEffect(scrollToBottom); // Scrool to bottom
  let { massageseHistory } = useSelector((state) => state.othercontacts);
  const { idSubsccribe, DialogId } = useSelector(
    (state) => state.othercontacts,
  );
  const { idUser } = useSelector((state) => state.people);
  massageseHistory = massageseHistory.map((e, index) => {
    // eslint-disable-next-line camelcase
    const { message, sender_id } = e;
    return (
      // eslint-disable-next-line camelcase
      <MessageItem
        message={message}
        // eslint-disable-next-line camelcase
        sender_id={sender_id}
        idUser={idUser}
        index={index}
        key={Math.random().toString(36).substring(7)}
      />
    );
  });
  function onMessage(userId, message) {
    if (userId === idSubsccribe) {
      dispatch(
        getMessage({
          message: message.body,
          sender_id: userId,
        }),
      );
    } else {
      alert(message.body);
    }
  }
  QuickBlox.chat.onMessageListener = onMessage;
  const sendMessage = (e) => {
    e.preventDefault();
    const form = e.currentTarget;
    if (form.message_feald.value) {
      dispatch(
        SendMessage({
          data: {
            message: form.message_feald.value,
            sender_id: idUser,
          },
          send: {
            DialogId,
            massage: form.message_feald.value,
            idSubsccribe,
          },
        }),
      );
    }
    form.message_feald.value = '';
  };
  return (
    <div className="RightChat">
      <div className="massagese">
        {massageseHistory || 'No Massage yet'}
        <div ref={messagesEndRef} className="end" />
      </div>
      <form action="" onSubmit={sendMessage}>
        <textarea
          name="message_feald"
          className="message_feald"
          id="message_feald"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
          placeholder="Type a message"
        />
        <button type="submit" className="myButton">
          Send Message
        </button>
      </form>
    </div>
  );
};
export default RightBar;
