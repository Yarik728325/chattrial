import React from 'react';
import './style.scss';

// eslint-disable-next-line camelcase
const MessageItem = ({ idUser, message, sender_id }) => {
  // eslint-disable-next-line camelcase
  if (idUser !== sender_id) {
    return (
      <>
        <div className="massageInChat">
          <div className="messageClient">
            <span>{message}</span>
          </div>
        </div>
      </>
    );
  }
  return (
    <>
      <div className="massageInChat">
        <div className="messageManager">
          <span>{message}</span>
        </div>
      </div>
    </>
  );
};
export default MessageItem;
