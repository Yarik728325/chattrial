import React from 'react';
import { useSelector } from 'react-redux';
// eslint-disable-next-line import/no-unresolved
import history from '@/utils/routing';
import LeftBar from '../LeftBar';
import RightBar from '../RightBar';
import './style.scss';

const App = () => {
  const { isAuth, data } = useSelector((state) => state.people);
  if (!isAuth) {
    history.push('/login');
  }
  return (
    <div className="wrapper-App">
      <LeftBar name={data?.username} />
      <RightBar />
    </div>
  );
};

export default App;
