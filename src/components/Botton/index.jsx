import React from 'react';
import './style.scss';
import { useDispatch, useSelector } from 'react-redux';
// eslint-disable-next-line import/no-unresolved
import { loadPeopleListActions } from '@/redux/actions/people';

const Botton = ({ text }) => {
  const { idUser } = useSelector((state) => state.people);
  const dispatch = useDispatch();
  return (
    <button
      type="button"
      className="botton"
      onClick={() => {
        dispatch(loadPeopleListActions(idUser));
      }}
    >
      {text}
    </button>
  );
};
export default Botton;
