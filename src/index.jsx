import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
// eslint-disable-next-line import/no-unresolved
import store from '@/redux/store';
// import { ConnectedRouter } from 'connected-react-router';
import RouterSwitch from './routes';
import 'antd/dist/antd.css';

ReactDOM.render(
  <Provider store={store}>
    <RouterSwitch />
  </Provider>,
  document.getElementById('root'),
);
