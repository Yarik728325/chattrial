import React from 'react';
import './style.scss';
import { Form, Input, Button, Checkbox } from 'antd';
import { tryLogIn } from '@/redux/actions/people';
import { useDispatch, useSelector } from 'react-redux';
import QuickBlox from 'quickblox/quickblox.min';
import history from '@/utils/routing';

const CREDENTIALS = {
  appId: 93882,
  authKey: 'BGGkMmDAnk97gcw',
  authSecret: '4gUyScDKuHxMt56',
  accountKey: '7x6sYUMC6_p4zujAmDJH',
};
QuickBlox.init(
  CREDENTIALS.appId,
  CREDENTIALS.authKey,
  CREDENTIALS.authSecret,
  CREDENTIALS.accountKey,
);
const Login = () => {
  const { loading, isAuth } = useSelector((state) => state.people);
  let checker = true;
  if (isAuth !== null && isAuth === false) {
    checker = false;
  }
  const dispatch = useDispatch();
  const onFinish = (values) => {
    dispatch(tryLogIn(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  if (loading) {
    return <h1>Loading....</h1>;
  }
  return (
    <div className="wrapper">
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <span>{checker ? '' : 'Incorrect login/password'}</span>
          <Button type="primary" htmlType="submit">
            LogIn
          </Button>
          <Button
            type="primary"
            onClick={() => {
              history.push('/createAccount');
            }}
          >
            createAccount
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
