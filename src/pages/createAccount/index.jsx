import React from 'react';
import './style.scss';
import { Form, Input, Button, Checkbox } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { createAccountActions } from '@/redux/actions/people';

const CreateAccount = () => {
  const dispatch = useDispatch();
  const onFinish = (values) => {
    dispatch(createAccountActions(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  const { loading, iscreate } = useSelector((state) => state.createUser);
  if (loading) {
    return <h1>Loading</h1>;
  }
  let checker;
  if (iscreate != null && !iscreate) {
    checker = 'Already have such login';
  } else if (iscreate !== null) {
    checker = 'Successful create';
  }
  return (
    <div className="wrapper">
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button type="primary" htmlType="submit">
            createAccount
          </Button>
          <span>{checker}</span>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CreateAccount;
