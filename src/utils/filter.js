const Dopfunction = ({ payload_, type_, state }) => {
  let tmpDate = state.tmp;
  let filterValue = '';
  if (type_ === 'pagination') {
    if (!state.multiselect) {
      return payload_;
    }
    if (state.multiselect.length > 0) {
      tmpDate = payload_;
      filterValue = state.multiselect;
      console.log(filterValue);
      tmpDate = tmpDate.filter((e) =>
        filterValue.find((a) => e.nat === a.value),
      );
    } else {
      return payload_;
    }
    return tmpDate;
  }
  if (type_ === 'multi_search') filterValue = payload_;
  else filterValue = state.multiselect;
  if (filterValue.length > 0) {
    tmpDate = tmpDate.filter((e) => filterValue.find((a) => e.nat === a.value));
  } else {
    tmpDate = state.tmp;
  }
  return tmpDate;
};
const filterHandler = ({ payload_, type_, state }) => {
  let tmpDate = state.tmp;
  let filterValue = '';
  tmpDate = Dopfunction({ payload_, type_, state });
  if (state.search.trim().length || payload_.length) {
    if (type_ === 'search') filterValue = payload_;
    else filterValue = state.search;

    if (filterValue.length) {
      tmpDate = tmpDate.filter((e) => {
        return e.name.first.indexOf(filterValue) > -1;
      });
    }
  }

  if (state.gender.trim().length || payload_.length) {
    if (type_ === 'gender') filterValue = payload_;
    else filterValue = state.gender;

    if (filterValue.length && filterValue !== 'all') {
      tmpDate = tmpDate.filter((e) => {
        return e.gender === filterValue;
      });
    }
  }

  return tmpDate;
};

export default filterHandler;
