const countNat = ({ state, _filter, _checker }) => {
  if (_checker === 'gender') {
    return state.filter((e) => {
      return e.gender === _filter;
    }).length;
  }
  return state.filter((e) => {
    return e.nat === _filter;
  }).length;
};

export default countNat;
