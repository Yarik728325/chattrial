import QuickBlox from 'quickblox/quickblox.min';

export const createSession = (params) =>
  new Promise((resolve) => {
    QuickBlox.createSession(params, function my(error, result) {
      if (!error) {
        resolve(result);
      } else {
        resolve(error);
      }
    });
  });
export const connetToChat = (params) =>
  new Promise((resolve, reject) => {
    QuickBlox.chat.connect(params, function my(error, contactList) {
      if (!error) {
        resolve(contactList);
      } else {
        reject(error);
      }
    });
  });
export const createUserconnect = (params) =>
  new Promise((resolve) => {
    // eslint-disable-next-line sonarjs/no-identical-functions
    QuickBlox.users.create(params, function some(error, result) {
      if (!error) {
        resolve(result);
      } else {
        resolve(error);
      }
    });
  });
export const createSessionWithout = () =>
  new Promise((resolve) => {
    QuickBlox.createSession(function (error, result) {
      if (!error) {
        resolve(result);
      }
    });
  });
