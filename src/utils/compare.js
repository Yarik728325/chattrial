const Compare = (state) => {
  if (state.checker) {
    return state.paginationUser.sort((a, b) =>
      a.name.first.localeCompare(b.name.first),
    );
  }

  return state.paginationUser.sort((a, b) =>
    b.name.first.localeCompare(a.name.first),
  );
};

export default Compare;
