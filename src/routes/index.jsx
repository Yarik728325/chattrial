import React from 'react';
import { Route, Router } from 'react-router';
// eslint-disable-next-line import/no-unresolved
import history from '@/utils/routing';
// eslint-disable-next-line import/no-unresolved
import App from '@/components/App';
// eslint-disable-next-line import/no-unresolved
import Login from '@/pages/Login';
import CreateAccount from '@/pages/createAccount';

const routes = [
  {
    id: 'main',
    path: '/',
    exact: true,
    component: App,
  },
  {
    id: 'login',
    path: '/login',
    exact: true,
    component: Login,
  },
  {
    id: 'createAccout',
    path: '/createAccount',
    exact: true,
    component: CreateAccount,
  },
];

const RouterSwitch = () => {
  return (
    <Router history={history}>
      {routes.map((e) => {
        const { id, ...props } = e;
        // eslint-disable-next-line react/jsx-props-no-spreading
        return <Route key={id} {...props} />;
      })}
    </Router>
  );
};

export default RouterSwitch;
