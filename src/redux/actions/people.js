// eslint-disable-next-line import/no-unresolved
import { createAction } from '@/utils/createAction';

export const loadPeople = createAction('PEOPLE/LOADPEOPLE');
export const loadSuccess = createAction('PEOPLE/LOADSUCCESS');
export const loadFailure = createAction('PEOPLE/LOADFAILURE');
export const loadPeopleListActions = createAction('PEOPLE/LIST');
export const wrongLogIn = createAction('POPLE/WRONGLOGIN');
export const tryLogIn = createAction('PEOPLE/TRYLOGIN');
export const createAccountActions = createAction('PEOPLE/CREATEACCOUNT');
export const isCreate = createAction('PEOPLE/ISCREATE');
