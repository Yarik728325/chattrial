// eslint-disable-next-line import/no-unresolved
import { createAction } from '@/utils/createAction';

export const LoadSuccessDeteils = createAction('DETEILS/LOADSUCCESSDETEILS');
export const LoadFailureDeteils = createAction('PEOPLE/LOADFAILUREDETEILS');
export const createChat = createAction('PEOPLE/CREATECHAT');
export const loadHistoryChat = createAction('PEOPLE/HISTORYCHAT');
export const SendMessage = createAction('PROPLE/SENDMESSAGE');
export const getMessage = createAction('PEOPLE/GETMESSAGe');
