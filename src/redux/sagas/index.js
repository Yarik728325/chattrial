import { all, spawn } from 'redux-saga/effects';
import peopleSaga from './PeopleSaga';
import loadingSaga from './DeteilsSaga';
import chatSaga from './Chats';

export default function* rootSaga() {
  const sagas = [peopleSaga, loadingSaga, chatSaga];

  yield all(sagas.map((s) => spawn(s)));
}
