// eslint-disable-next-line import/no-extraneous-dependencies
import {
  call,
  // apply,
  put,
  takeLatest,
  // put,
  // eslint-disable-next-line import/no-extraneous-dependencies
} from '@redux-saga/core/effects';
// eslint-disable-next-line import/no-unresolved
import QuickBlox from 'quickblox/quickblox.min';
// eslint-disable-next-line import/no-unresolved
import {
  createChat,
  loadHistoryChat,
  SendMessage,
} from '@/redux/actions/deteils';

const createPromiseChat = (params) =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line sonarjs/no-identical-functions
    QuickBlox.chat.dialog.create(params, function (error, dialog) {
      if (!error) {
        resolve(dialog);
      } else reject(error);
    });
  });
const dialogHistory = (params) =>
  new Promise((resolve, reject) => {
    const data = {
      chat_dialog_id: params,
      sort_desc: 'date_sent',
      limit: 100,
      skip: 0,
    };
    QuickBlox.chat.message.list(data, function (error, messages) {
      if (!error) {
        resolve(messages.items);
      } else {
        reject(error);
      }
    });
  });

export function* createDialogs({ payload }) {
  const params = {
    type: 3,
    occupants_ids: [+payload.id],
  };
  const dialogs = yield call(createPromiseChat, params);
  const { _id } = dialogs;
  let historyMessage = yield call(dialogHistory, _id);
  historyMessage = historyMessage.reverse();
  yield put(
    loadHistoryChat({ data: historyMessage, id: payload.id, DialogId: _id }),
  );
}
export function* sendingMessage({ payload }) {
  const message = {
    type: 'chat',
    body: payload.send.massage,
    extension: {
      save_to_history: 1,
      dialog_id: payload.send.DialogId,
    },
    markable: 1,
  };

  const opponentId = payload.send.idSubsccribe;
  try {
    message.id = QuickBlox.chat.send(opponentId, message);
  } catch (e) {
    if (e.name === 'ChatNotConnectedError') {
      // not connected to chat
    }
  }
  yield console.log(payload.send);
}
export default function* chatSaga() {
  yield takeLatest(createChat.type, createDialogs);
  yield takeLatest(SendMessage.type, sendingMessage);
}
