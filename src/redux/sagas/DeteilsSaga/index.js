// eslint-disable-next-line import/no-extraneous-dependencies
import {
  call,
  put,
  // apply,
  // put,
  takeLatest,
  // put,
  // eslint-disable-next-line import/no-extraneous-dependencies
} from '@redux-saga/core/effects';
// eslint-disable-next-line import/no-unresolved
import QuickBlox from 'quickblox/quickblox.min';
// eslint-disable-next-line import/no-unresolved
import { loadPeopleListActions } from '@/redux/actions/people';
// eslint-disable-next-line import/no-unresolved
import { LoadSuccessDeteils } from '@/redux/actions/deteils';

const loadingList = (params) =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line sonarjs/no-identical-functions
    QuickBlox.users.listUsers(params, function my(error, result) {
      if (!error) {
        resolve(result);
      } else {
        reject(error);
      }
    });
  });

export function* loadPeopleList({ payload }) {
  const params = {
    filter: {
      field: 'created_at',
      param: 'between',
      value: '2021-01-01, 2021-11-06',
    },
    order: {
      field: 'created_at',
      sort: 'asc',
    },
    page: 1,
    per_page: 50,
  };
  let { items } = yield call(loadingList, params);
  items = items.filter((e) => e.user.id !== payload);
  yield put(LoadSuccessDeteils(items));
}
export default function* loadingSaga() {
  yield takeLatest(loadPeopleListActions.type, loadPeopleList);
}
