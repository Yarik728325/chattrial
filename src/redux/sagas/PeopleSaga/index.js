// eslint-disable-next-line import/no-extraneous-dependencies
import {
  call,
  put,
  // apply,
  // put,
  takeLatest,
  // put,
  // eslint-disable-next-line import/no-extraneous-dependencies
} from '@redux-saga/core/effects';
import {
  tryLogIn,
  loadSuccess,
  wrongLogIn,
  loadPeople,
  createAccountActions,
  isCreate,
} from '@/redux/actions/people';
// eslint-disable-next-line import/no-unresolved
import history from '@/utils/routing';
import {
  createSessionWithout,
  createUserconnect,
  createSession,
  connetToChat,
} from '@/utils/people';

export function* createUser({ payload }) {
  const { username, password } = payload;
  const params = {
    login: username,
    password,
  };
  yield call(createSessionWithout);
  const check = yield call(createUserconnect, params);
  if (check.code === 422) {
    yield put(isCreate(false));
  } else {
    yield put(isCreate(true));
  }
}

export function* connectUser({ payload }) {
  const { username, password } = payload;
  const params = {
    login: username,
    password,
  };
  const session = yield call(createSession, params);
  if (session.code === 401) {
    yield put(wrongLogIn());
  }
  // eslint-disable-next-line no-debugger
  else {
    yield put(loadPeople());
    const userCredentials = {
      userId: session.user_id,
      password,
    };
    const connect = yield call(connetToChat, userCredentials);
    const data = {
      session,
      connect,
      username,
    };
    yield put(loadSuccess({ data, userId: session.user_id }));
    yield history.push('/');
  }
}
export default function* peopleSaga() {
  yield takeLatest(tryLogIn.type, connectUser);
  yield takeLatest(createAccountActions.type, createUser);
}
