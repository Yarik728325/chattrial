// eslint-disable-next-line import/no-unresolved
import {
  LoadSuccessDeteils,
  LoadFailureDeteils,
  loadHistoryChat,
  SendMessage,
  getMessage,
  createChat,
  // eslint-disable-next-line import/no-unresolved
} from '@/redux/actions/deteils';

const initinalState = {
  users: null,
  loading: true,
  error: false,
  massageseHistory: [],
  idSubsccribe: null,
  DialogId: null,
  indexUser: -1,
};

export default (state = initinalState, { type, payload }) => {
  switch (type) {
    case createChat.type:
      return {
        ...state,
        indexUser: payload.index,
      };
    case SendMessage.type:
      return {
        ...state,
        massageseHistory: [...state.massageseHistory, payload.data],
      };
    case getMessage.type:
      return {
        ...state,
        massageseHistory: [...state.massageseHistory, payload],
      };
    case LoadSuccessDeteils.type:
      return {
        ...state,
        loading: false,
        users: payload,
      };
    case LoadFailureDeteils.type:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    case loadHistoryChat.type:
      return {
        ...state,
        idSubsccribe: payload.id,
        massageseHistory: payload.data,
        DialogId: payload.DialogId,
      };
    default:
      return state;
  }
};
