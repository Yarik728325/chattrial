import { combineReducers } from 'redux';

import people from './people';
import othercontacts from './othercontacts';
import createUser from './createUser';

export default combineReducers({
  people,
  othercontacts,
  createUser,
});
