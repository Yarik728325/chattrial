// eslint-disable-next-line import/no-unresolved
import {
  loadPeople,
  loadSuccess,
  loadFailure,
  wrongLogIn,
} from '@/redux/actions/people';

const initinalState = {
  loading: null,
  error: false,
  data: null,
  isAuth: null,
  idUser: null,
};

export default (state = initinalState, { type, payload }) => {
  switch (type) {
    case wrongLogIn.type:
      return {
        ...state,
        isAuth: false,
      };
    case loadPeople.type:
      return {
        ...state,
        loading: true,
      };
    case loadSuccess.type:
      return {
        ...state,
        isAuth: true,
        data: payload.data,
        loading: false,
        idUser: payload.userId,
      };
    case loadFailure.type:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
};
