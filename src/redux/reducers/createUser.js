// eslint-disable-next-line import/no-unresolved
import { isCreate, createAccountActions } from '@/redux/actions/people';

const initinalState = {
  loading: false,
  iscreate: null,
};

export default (state = initinalState, { type, payload }) => {
  switch (type) {
    case createAccountActions.type:
      return {
        ...state,
        loading: true,
      };
    case isCreate.type:
      return {
        ...state,
        loading: false,
        iscreate: payload,
      };
    default:
      return state;
  }
};
