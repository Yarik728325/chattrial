import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
// eslint-disable-next-line import/no-unresolved
import history from '@/utils/routing';
import reducers from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middlewares = [sagaMiddleware];

if (process.env.NODE_ENV !== 'production') {
  middlewares.push(logger);
}

const enhancers = composeEnhancers(
  applyMiddleware(...middlewares, routerMiddleware(history)),
);
const store = createStore(reducers, enhancers);
sagaMiddleware.run(rootSaga);
export default store;
